<?php
namespace Updashd\Worker;

use JJG\Ping;
use Updashd\Configlib\Config;
use Updashd\Configlib\Validator\HostnameValidator;
use Updashd\Configlib\Validator\Ip4Validator;
use Updashd\Configlib\Validator\NumberValidator;
use Updashd\Configlib\Validator\PortNumberValidator;

class IcmpPing implements WorkerInterface {
    const CONFIG_FIELD_HOSTNAME = 'hostname';
    const CONFIG_FIELD_IP = 'ip';
    const CONFIG_FIELD_PREFER_IP = 'prefer_ip';
    const CONFIG_FIELD_TTL = 'ttl';
    const CONFIG_FIELD_TIMEOUT = 'timeout';
    const CONFIG_FIELD_METHOD = 'method';
    const CONFIG_FIELD_PORT = 'port';

    const CONFIG_METHOD_OPTION_EXEC = 'exec';
    const CONFIG_METHOD_OPTION_FSOCKOPEN = 'fsockopen';
    // const CONFIG_METHOD_OPTION_SOCKET = 'socket'; // Unavailable since it requires root.

    const CONFIG_TTL_OPTION_SAME_HOST = 0;
    const CONFIG_TTL_OPTION_SAME_SUBNET = 1;
    const CONFIG_TTL_OPTION_SAME_SITE = 32;
    const CONFIG_TTL_OPTION_SAME_REGION = 64;
    const CONFIG_TTL_OPTION_SAME_CONTINENT = 128;
    const CONFIG_TTL_OPTION_UNRESTRICTED = 255;

    const METRIC_FIELD_LATENCY = 'latency';
    const METRIC_FIELD_HOST_UP = 'host_up';

    const GROUP_NETWORK = 'network';

    protected $ping;
    protected $method;

    const GROUP_MISC = 'misc';

    public static function getReadableName () {
        return 'ICMP Ping';
    }

    public static function getServiceName () {
        return 'icmp_ping';
    }

    /**
     * Create and return a Config object for this service
     * @return Config
     */
    public static function createConfig () {
        $config = new Config();

        $config->addFieldText(self::CONFIG_FIELD_HOSTNAME, 'Host Name', null, true)
            ->setReferenceField('hostname')
            ->addValidator(new HostnameValidator());

        $config->addFieldText(self::CONFIG_FIELD_IP, 'IP')
            ->setReferenceField('ip')
            ->addValidator(new Ip4Validator());;

        $config->addFieldCheckbox(self::CONFIG_FIELD_PREFER_IP, 'Prefer IP', false, true);

        $config->addFieldNumber(self::CONFIG_FIELD_TIMEOUT, 'Timeout', 10, true)
            ->addValidator(new NumberValidator(false, 1, 120));

        $config->addFieldSelect(self::CONFIG_FIELD_TTL, 'TTL', self::getTtlOptions(), self::CONFIG_TTL_OPTION_UNRESTRICTED, true);

        $config->addFieldSelect(self::CONFIG_FIELD_METHOD, 'Method', self::getMethodOptions(), self::CONFIG_METHOD_OPTION_EXEC, true);

        $config->addFieldNumber(self::CONFIG_FIELD_PORT, 'Port', 80, true)
            ->addValidator(new PortNumberValidator());

        $config->addGroup(self::GROUP_NETWORK, 'Network');
        $config->addFieldToGroup(self::GROUP_NETWORK, self::CONFIG_FIELD_HOSTNAME);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::CONFIG_FIELD_IP);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::CONFIG_FIELD_PREFER_IP);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::CONFIG_FIELD_TIMEOUT);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::CONFIG_FIELD_TTL);

        $config->addGroup(self::GROUP_MISC, 'Miscellaneous');
        $config->addFieldToGroup(self::GROUP_MISC, self::CONFIG_FIELD_METHOD);
        $config->addFieldToGroup(self::GROUP_MISC, self::CONFIG_FIELD_PORT);

        return $config;
    }

    protected static function getMethodOptions () {
        return [
            self::CONFIG_METHOD_OPTION_EXEC => 'ICMP Ping (System)',
            self::CONFIG_METHOD_OPTION_FSOCKOPEN => 'TCP Ping (Port Required)',
            // self::CONFIG_METHOD_OPTION_SOCKET => 'ICMP Ping (Raw)' // This option will not be available, as it requires root.
        ];
    }

    protected static function getTtlOptions () {
        return [
            self::CONFIG_TTL_OPTION_SAME_HOST => 'Same Host',
            self::CONFIG_TTL_OPTION_SAME_SUBNET => 'Same Subnet',
            self::CONFIG_TTL_OPTION_SAME_SITE => 'Same Site',
            self::CONFIG_TTL_OPTION_SAME_REGION => 'Same Region',
            self::CONFIG_TTL_OPTION_SAME_CONTINENT => 'Same Continent',
            self::CONFIG_TTL_OPTION_UNRESTRICTED => 'Unrestricted'
        ];
    }

    /**
     * Create and return a Result object for this service
     * @return Result
     */
    public static function createResult () {
        $result = new Result();

        $result->addMetricFloat(self::METRIC_FIELD_LATENCY, 'Latency', 'ms');

        $result->addMetricInt(self::METRIC_FIELD_HOST_UP, 'Received Response');

        return $result;
    }

    /**
     * Create a worker instance for the given service
     * @param Config $config the Config object used for configuration
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public function __construct (Config $config) {
        $preferIp = $config->getValueRequired(self::CONFIG_FIELD_PREFER_IP);

        $hostOrIp = $config->getValueRequired($preferIp ? self::CONFIG_FIELD_IP : self::CONFIG_FIELD_HOSTNAME);

        $ping = new Ping($hostOrIp);

        $ping->setTimeout($config->getValue(self::CONFIG_FIELD_TIMEOUT, true));
        $ping->setTtl($config->getValue(self::CONFIG_FIELD_TTL, true));

        $method = $config->getValueRequired(self::CONFIG_FIELD_METHOD);
        $this->setMethod($method);

        if ($method == self::CONFIG_METHOD_OPTION_FSOCKOPEN) {
            $ping->setPort($config->getValueRequired(self::CONFIG_FIELD_PORT));
        }

        $this->setPing($ping);
    }

    /**
     * Run the given test
     * @throws \Updashd\Worker\Exception\WorkerRuntimeException
     * @return Result the results of the test
     */
    public function run () {
        $result = self::createResult();

        $ping = $this->getPing();

        $latency = $ping->ping($this->getMethod());

        if ($latency !== false) {
            $result->setStatus($result::STATUS_SUCCESS);
            $result->setMetricValue(self::METRIC_FIELD_HOST_UP, 1);
            $result->setMetricValue(self::METRIC_FIELD_LATENCY, $latency);
        }
        else {
            $result->setStatus($result::STATUS_CRITICAL);
            $result->setMetricValue(self::METRIC_FIELD_HOST_UP, 0);
            $result->setErrorCode(0);
            $result->setErrorMessage('Could not reach host.');
        }

        return $result;
    }

    /**
     * @return Ping
     */
    public function getPing () {
        return $this->ping;
    }

    /**
     * @param Ping $ping
     */
    public function setPing (Ping $ping) {
        $this->ping = $ping;
    }

    /**
     * @return string
     */
    public function getMethod () {
        return $this->method;
    }

    /**
     * @param string $method One of the constants CONFIG_OPTION_METHOD_*
     */
    public function setMethod ($method) {
        $this->method = $method;
    }
}