<?php
use Updashd\Worker\IcmpPing;

require __DIR__ . '/../vendor/autoload.php';


$config = IcmpPing::createConfig();

$config->setValue(IcmpPing::CONFIG_FIELD_HOSTNAME, 'test.updashd.com');
$config->setValue(IcmpPing::CONFIG_FIELD_METHOD, IcmpPing::CONFIG_METHOD_OPTION_EXEC);

foreach ($config->getFields() as $field) {
    echo $field->getLabel() . ': ' . $field->getValue(true) . PHP_EOL;
}

$pingWorker = new IcmpPing($config);

$result = $pingWorker->run();

print_r($result->toArray());